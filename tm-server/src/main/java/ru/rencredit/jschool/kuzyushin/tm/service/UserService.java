package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IUserRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IUserService;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.*;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.Role;
import ru.rencredit.jschool.kuzyushin.tm.repository.UserRepository;
import ru.rencredit.jschool.kuzyushin.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

public class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public void remove(@NotNull User user) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.remove(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void persist(final @Nullable User user) {
        if (user == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.persist(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(final @Nullable User user) {
        if (user == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.merge(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void create(final @Nullable String login, final @Nullable String password, final @Nullable String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        persist(user);
    }

    @Override
    public void create(final @Nullable String login, final @Nullable String password, final @Nullable Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        persist(user);
    }

    @Override
    public @NotNull List<UserDTO> findAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        final List<User> users = repository.findAll();
        entityManager.close();
        return UserDTO.toDTO(users);
    }

    @Override
    public @Nullable User findById(final @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        final User user = repository.findById(id);
        entityManager.close();
        return user;
    }

    @Override
    public @Nullable User findByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        final User user = repository.findByLogin(login);
        entityManager.close();
        return user;
    }

    @Override
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeById(id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeByLogin(login);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateLogin(final @Nullable String id, final @Nullable String login) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findById(id);
        user.setLogin(login);
        merge(user);
    }

    @Override
    public void updatePassword(final @Nullable String id, final @Nullable String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(id);
        user.setPasswordHash(password);
        merge(user);
    }

    @Override
    public void updateEmail(final @Nullable String id, final @Nullable String email) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final User user = findById(id);
        user.setEmail(email);
        merge(user);
    }

    @Override
    public void updateFirstName(final @Nullable String id, final @Nullable String firstName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        user.setFirstName(firstName);
        merge(user);
    }

    @Override
    public void updateLastName(final @Nullable String id, final @Nullable String lastName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        user.setLastName(lastName);
        merge(user);
    }

    @Override
    public void updateMiddleName(final @Nullable String id, final @Nullable String middleName) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        user.setMiddleName(middleName);
        merge(user);
    }

    @Override
    public void lockUserByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        user.setLocked(true);
        merge(user);
    }

    @Override
    public void unlockUserByLogin(final @Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        user.setLocked(false);
        merge(user);
    }

    @Override
    public void load(final @Nullable List<UserDTO> users) {
        if (users == null) return;
        for (final UserDTO userDTO: users){
            @NotNull final User user = new User();
            user.setLogin(userDTO.getLogin());
            user.setFirstName(userDTO.getFirstName());
            user.setLastName(userDTO.getLastName());
            user.setMiddleName(userDTO.getMiddleName());
            user.setId(userDTO.getId());
            user.setPasswordHash(userDTO.getPasswordHash());
            user.setEmail(userDTO.getEmail());
            user.setRole(userDTO.getRole());
            user.setLocked(userDTO.getLocked());
            persist(user);
        }
    }
}
