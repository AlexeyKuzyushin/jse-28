package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.repository.IProjectRepository;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.dto.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.TaskDTO;
import ru.rencredit.jschool.kuzyushin.tm.entity.Task;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyDateException;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyIdException;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyUserIdException;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyNameException;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.exception.system.IncorrectStartDateException;
import ru.rencredit.jschool.kuzyushin.tm.repository.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    public ProjectService(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public Long count() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final Long countOfProjects = repository.count();
        entityManager.close();
        return countOfProjects;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectDTO> projectsDTO = ProjectDTO.toDTO(repository.findAll());
        entityManager.close();
        return projectsDTO;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectDTO> projectsDTO = ProjectDTO.toDTO(repository.findAll(userId));
        entityManager.close();
        return projectsDTO;
    }

    @Override
    public void create(final @Nullable String userId,
                       final @Nullable String name,
                       final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        project.setUser(serviceLocator.getUserService().findById(userId));
        persist(project);
    }

    @Nullable
    @Override
    public Project findById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @Nullable final Project project = repository.findById(userId, id);
        entityManager.close();
        return project;
    }

    @Nullable
    @Override
    public Project findByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        @Nullable final Project project = repository.findByName(userId, name);
        entityManager.close();
        return project;
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(final @Nullable String userId, final @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeByName(final @Nullable String userId, final @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.removeByName(userId, name);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Project updateById(
            final @Nullable String userId, final @Nullable String id,
            final @Nullable String name, final @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable Project project = findById(userId, id);
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public void updateStartDate(final @Nullable String userId, final @Nullable String id, final @Nullable Date date) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Project project = findById(userId, id);
        if (date.before(new Date(System.currentTimeMillis()))) throw new IncorrectStartDateException(date);
        project.setStartDate(date);
        merge(project);
    }

    @Override
    public void updateFinishDate(final @Nullable String userId, final @Nullable String id, final @Nullable Date date) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (date == null) throw new EmptyDateException();
        @Nullable final Project project = findById(userId, id);
        assert project != null;
        if (project.getStartDate() == null) throw new IncorrectStartDateException();
        if (project.getStartDate().after(date)) throw new IncorrectStartDateException(date);
        project.setFinishDate(date);
        merge(project);

    }

    @Override
    public void remove(final @NotNull Project project) {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.remove(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void persist(final @Nullable Project project) {
        if (project == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.persist(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void merge(final @Nullable Project project) {
        if (project == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlConnectionService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            repository.merge(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void load(final @Nullable List<ProjectDTO> projects) {
        if (projects == null) return;
        clear();
        for (final ProjectDTO projectDTO: projects){
            @NotNull final Project project = new Project();
            project.setName(projectDTO.getName());
            project.setDescription(projectDTO.getDescription());
            project.setUser(serviceLocator.getUserService().findById(projectDTO.getUserId()));
            project.setId(projectDTO.getId());
            project.setStartDate(projectDTO.getStartDate());
            project.setFinishDate(projectDTO.getFinishDate());
            project.setCreationTime(projectDTO.getCreationDate());
            persist(project);
        }
    }
}
