package ru.rencredit.jschool.kuzyushin.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.api.endpoint.IUserEndpoint;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IServiceLocator;
import ru.rencredit.jschool.kuzyushin.tm.dto.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@NoArgsConstructor
public final class UserEndpoint implements IUserEndpoint {

    private IServiceLocator serviceLocator;

    public UserEndpoint(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void updateUserLogin(
            @WebParam(name = "session", partName = "session") final @Nullable SessionDTO sessionDTO,
            @WebParam(name = "login", partName = "login") @Nullable String login) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().updateLogin(sessionDTO.getUserId(), login);
    }

    @Override
    @WebMethod
    public void updateUserPassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "password", partName = "password") @Nullable String password) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().updatePassword(sessionDTO.getUserId(), password);
    }

    @Override
    @WebMethod
    public void updateUserEmail(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "email", partName = "email") @Nullable String email) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().updateEmail(sessionDTO.getUserId(), email);
    }

    @Nullable
    @Override
    @WebMethod
    public void updateUserFirstName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "firstName", partName = "firstName") @Nullable String firstName) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().updateFirstName(sessionDTO.getUserId(), firstName);
    }

    @Override
    @WebMethod
    public void updateUserLastName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "lastName", partName = "lastName") @Nullable String lastName) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().updateLastName(sessionDTO.getUserId(), lastName);
    }

    @Override
    @WebMethod
    public void updateUserMiddleName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO,
            @WebParam(name = "middleName", partName = "middleName") @Nullable String middleName) {
        serviceLocator.getSessionService().validate(sessionDTO);
        serviceLocator.getUserService().updateMiddleName(sessionDTO.getUserId(), middleName);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO viewUserProfile(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO sessionDTO) {
        serviceLocator.getSessionService().validate(sessionDTO);
        return serviceLocator.getSessionService().getUser(sessionDTO);
    }
}
