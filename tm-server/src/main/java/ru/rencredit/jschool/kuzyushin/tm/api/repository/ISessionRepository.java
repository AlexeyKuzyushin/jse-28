package ru.rencredit.jschool.kuzyushin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull
    Long count();

    @NotNull
    List<Session> findAll(@NotNull String userId);

    @NotNull
    List<Session> findAll();

    @Nullable
    Session findById(@NotNull String id);

    @Nullable
    Session findByUserId(@NotNull String userId);

    void clear();

    void removeAll(@NotNull String userId);

    void removeById(@NotNull String id);

    void removeByUserId(@NotNull String userId);

    boolean contains(@NotNull String id);
}
