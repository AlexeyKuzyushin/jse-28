package ru.rencredit.jschool.kuzyushin.tm.test.service;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.rencredit.jschool.kuzyushin.tm.api.service.IProjectService;
import ru.rencredit.jschool.kuzyushin.tm.entity.Project;
import ru.rencredit.jschool.kuzyushin.tm.marker.UnitCategory;
import ru.rencredit.jschool.kuzyushin.tm.service.ProjectService;

public class ProjectServiceTest {

//    final private IProjectService projectService = new ProjectService();
//
//    final private static Project testProject = new Project();
//
//    @BeforeClass
//    public static void initProject() {
//        testProject.setName("projectOne");
//        testProject.setUserId("123123");
//        testProject.setDescription("testProject");
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void addTest() {
//        projectService.add(testProject.getUserId(), testProject);
//        Assert.assertEquals(testProject, projectService.findById(testProject.getUserId(), testProject.getId()));
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void removeTest() {
//        projectService.add(testProject.getUserId(), testProject);
//        Assert.assertFalse(projectService.findAll(testProject.getUserId()).isEmpty());
//        projectService.remove(testProject.getUserId(), testProject);
//        Assert.assertNull(projectService.findById(testProject.getUserId(), testProject.getId()));
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void findAllTest() {
//        projectService.add(testProject.getUserId(), testProject);
//        projectService.add(testProject.getUserId(), testProject);
//        Assert.assertEquals(2, projectService.findAll(testProject.getUserId()).size());
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void clearTest() {
//        projectService.add(testProject.getUserId(), testProject);
//        Assert.assertEquals(1, projectService.findAll(testProject.getUserId()).size());
//        projectService.removeAllByUserId(testProject.getUserId());
//        Assert.assertEquals(0, projectService.findAll(testProject.getUserId()).size());
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void findByIdTest() {
//        projectService.add(testProject.getUserId(), testProject);
//        final Project project = projectService.findById(testProject.getUserId(), testProject.getId());
//        Assert.assertNotNull(project);
//        Assert.assertEquals(testProject.getId(), project.getId());
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void findByNameTest() {
//        projectService.add(testProject.getUserId(), testProject);
//        final Project project = projectService.findByName(testProject.getUserId(), testProject.getName());
//        Assert.assertNotNull(project);
//        Assert.assertEquals(testProject.getId(), project.getId());
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void findByIndexTest() {
//        projectService.add(testProject.getUserId(), testProject);
//        final Project project = projectService.findOneByIndex(testProject.getUserId(), 0);
//        Assert.assertNotNull(project);
//        Assert.assertEquals(testProject.getId(), project.getId());
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void removeByIdTest() {
//        projectService.add(testProject.getUserId(), testProject);
//        Assert.assertFalse(projectService.findAll(testProject.getUserId()).isEmpty());
//        projectService.removeById(testProject.getUserId(), testProject.getId());
//        Assert.assertNull(projectService.findById(testProject.getUserId(), testProject.getId()));
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void removeByNameTest() {
//        projectService.add(testProject.getUserId(), testProject);
//        Assert.assertFalse(projectService.findAll(testProject.getUserId()).isEmpty());
//        projectService.removeOneByName(testProject.getUserId(), testProject.getName());
//        Assert.assertNull(projectService.findById(testProject.getUserId(), testProject.getId()));
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void removeByIndexTest() {
//        projectService.add(testProject.getUserId(), testProject);
//        Assert.assertFalse(projectService.findAll(testProject.getUserId()).isEmpty());
//        projectService.removeOneByIndex(testProject.getUserId(), 0);
//        Assert.assertNull(projectService.findById(testProject.getUserId(), testProject.getId()));
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void updateByIdTest() {
//        projectService.add(testProject.getUserId(), testProject);
//        Assert.assertFalse(projectService.findAll(testProject.getUserId()).isEmpty());
//        projectService.updateById(testProject.getUserId(), testProject.getId(),
//                "project", "description");
//        final Project project = projectService.findById(testProject.getUserId(), testProject.getId());
//        Assert.assertEquals(project.getId(), testProject.getId());
//        Assert.assertEquals(project.getUserId(), testProject.getUserId());
//        Assert.assertEquals(project.getName(), "project");
//        Assert.assertEquals(project.getDescription(), "description");
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void updateByIndexTest() {
//        projectService.add(testProject.getUserId(), testProject);
//        Assert.assertFalse(projectService.findAll(testProject.getUserId()).isEmpty());
//        projectService.updateOneByIndex(testProject.getUserId(), 0,
//                "project", "description");
//        final Project project = projectService.findById(testProject.getUserId(), testProject.getId());
//        Assert.assertEquals(project.getId(), testProject.getId());
//        Assert.assertEquals(project.getUserId(), testProject.getUserId());
//        Assert.assertEquals(project.getName(), "project");
//        Assert.assertEquals(project.getDescription(), "description");
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void createWithoutDescriptionTest() {
//        projectService.create(testProject.getUserId(), testProject.getName());
//        final Project project = projectService.findByName(testProject.getUserId(), testProject.getName());
//        Assert.assertNotNull(project);
//        Assert.assertEquals(project.getUserId(), testProject.getUserId());
//        Assert.assertEquals(project.getName(), testProject.getName());
//    }
//
//    @Test
//    @Category(UnitCategory.class)
//    public void createWithDescriptionTest() {
//        projectService.create(testProject.getUserId(), testProject.getName(), testProject.getDescription());
//        final Project project = projectService.findByName(testProject.getUserId(), testProject.getName());
//        Assert.assertNotNull(project);
//        Assert.assertEquals(project.getUserId(), testProject.getUserId());
//        Assert.assertEquals(project.getName(), testProject.getName());
//        Assert.assertEquals(project.getDescription(), testProject.getDescription());
//    }
}
