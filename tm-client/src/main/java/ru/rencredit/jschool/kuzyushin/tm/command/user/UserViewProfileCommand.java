package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.UserDTO;

public final class UserViewProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-view-profile";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show user profile";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW USER PROFILE]");
        if (serviceLocator != null) {
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            @Nullable final UserDTO userDTO = serviceLocator.getUserEndpoint().viewUserProfile(sessionDTO);
            if (userDTO == null) return;
            System.out.println("ID: " + userDTO.getId());
            System.out.println("LOGIN: " + userDTO.getLogin());
            System.out.println("E-mail: " + userDTO.getEmail());
            System.out.println("FIRST NAME: " + userDTO.getFirstName());
            System.out.println("LAST NAME: " + userDTO.getLastName());
            System.out.println("MIDDLE NAME: " + userDTO.getMiddleName());
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
