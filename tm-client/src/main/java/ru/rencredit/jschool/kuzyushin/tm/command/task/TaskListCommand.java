package ru.rencredit.jschool.kuzyushin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.TaskDTO;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        System.out.println("[LIST TASKS]");
        if (serviceLocator != null) {
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            @Nullable final List<TaskDTO> tasksDTO = serviceLocator.getTaskEndpoint().findAllTasks(sessionDTO);
            int index = 1;
            for (TaskDTO taskDTO: tasksDTO) {
                System.out.println(index + ". " + taskDTO.getName());
                index++;
            }
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
