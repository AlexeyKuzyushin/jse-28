package ru.rencredit.jschool.kuzyushin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

public interface ISessionService {

    @Nullable
    SessionDTO getCurrentSession();

    void setCurrentSession(@Nullable SessionDTO currentSession);

    void clearCurrentSession();
}
