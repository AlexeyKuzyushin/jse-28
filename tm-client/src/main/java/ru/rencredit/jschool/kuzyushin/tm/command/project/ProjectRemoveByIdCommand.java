package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        if (serviceLocator != null) {
            @Nullable final String id = TerminalUtil.nextLine();
            @Nullable SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            serviceLocator.getProjectEndpoint().removeProjectById(sessionDTO, id);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
