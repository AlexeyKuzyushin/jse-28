package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.ProjectDTO;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;

import java.util.List;

public class ProjectListByUserId extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list-by-userId";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list of user";
    }

    @Override
    public void execute() {
        System.out.println("[LIST PROJECTS]");
        if (serviceLocator != null) {
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            @NotNull final List<ProjectDTO> projectsDTO = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(sessionDTO);
            int index = 1;
            for (ProjectDTO projectDTO: projectsDTO) {
                System.out.println(index + ". " + projectDTO.getName());
                index++;
            }
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
