package ru.rencredit.jschool.kuzyushin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("[ENTER NAME]");

        if (serviceLocator != null) {
            @Nullable final String name = TerminalUtil.nextLine();
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            System.out.println("[ENTER DESCRIPTION]");
            @Nullable final String description = TerminalUtil.nextLine();
            serviceLocator.getProjectEndpoint().createProject(sessionDTO, name, description);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
