package ru.rencredit.jschool.kuzyushin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.SessionDTO;
import ru.rencredit.jschool.kuzyushin.tm.util.TerminalUtil;

public final class UserUpdateFirstNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "user-update-first-name";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Update user first name";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE USER FIRST NAME]");
        if (serviceLocator != null) {
            @Nullable final SessionDTO sessionDTO = serviceLocator.getSessionService().getCurrentSession();
            System.out.println("ENTER FIRST NAME:");
            @Nullable final String firstName = TerminalUtil.nextLine();
            serviceLocator.getUserEndpoint().updateUserFirstName(sessionDTO, firstName);
            System.out.println("[OK]");
        }
        else System.out.println("[FAILED]");
    }
}
