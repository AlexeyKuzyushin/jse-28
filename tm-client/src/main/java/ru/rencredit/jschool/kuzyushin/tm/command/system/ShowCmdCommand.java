package ru.rencredit.jschool.kuzyushin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.rencredit.jschool.kuzyushin.tm.command.AbstractCommand;

import java.util.List;

public final class ShowCmdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Show application commands";
    }

    @Override
    public void execute() {

        if (serviceLocator != null) {
            @NotNull final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
            for (@NotNull final AbstractCommand command: commands)
                if (command.arg() != null)
                    System.out.println(command.name());
        }
        else System.out.println("[FAILED]");
    }
}
